from pcd8544.lcd            import  LCD_HEIGHT
from pcd8544.fonts.font_vi  import  font_vi

from MenuBarModel           import  wait_button

import codecs
import json

_DEFAULT   =   -1

class _Title():
    def __init__(self, text, font, padding):
        self.text       =   text
        self._height    =   font['HEIGHT'] + padding

class _Arrow():
    def __init__(self, byte_array, y_arrow, default_lcd):
        self.__img          =   byte_array
        self.__Y            =   y_arrow
        self.__X            =   78
        self.__HEIGHT       =   3
        self.__WIDTH        =   5

        self.__default_lcd  =   default_lcd

    def invert(self, lcd = None):
        lcd     =   lcd or self.__default_lcd

        lcd.invert_rect(x1 = self.__X, y1 = self.__Y, width = self.__WIDTH - 1, height = self.__HEIGHT - 1)
        lcd.refresh()

    def draw(self, lcd = None):
        lcd     =   lcd or self.__default_lcd

        lcd.draw_image(self.__img, width = self.__WIDTH, height = self.__HEIGHT, x = self.__X, y = self.__Y)

class MenuBar():
    __items             =   []
    __font              =   None
    __padding           =   None
    __default_lcd       =   None
    __default_buttons   =   None
    __table_id          =   1 #default table id = 1

    has_new_menu        =   False

    def __init__(self, item_name, default_lcd, default_buttons, font = font_vi, padding = 0):

        from MenuBarModel   import Model
        self.model              =   Model()

        self.title              =   _Title(item_name, font, padding)

        self.__font             =   font
        self.__padding          =   padding

        self.__default_lcd      =   default_lcd
        self.__default_buttons  =   default_buttons

        self.__current_item     =   0
        self.__window_position  =   0
        self.__items            =   []
        self.has_new_menu        =   False

        arrow_down              =   [0x01, 0x03, 0x07, 0x03, 0x01]  # byte array arrow down image, size 5x3
        arrow_up                =   [0x04, 0x06, 0x07, 0x06, 0x04]  # byte array arrow up image, size 5x3

        self._arrow_down        =   _Arrow(arrow_down, y_arrow = 44, default_lcd = self.__default_lcd)
        self._arrow_up          =   _Arrow(arrow_up, y_arrow = self.title._height + 1, default_lcd = self.__default_lcd)

    def show(self, lcd = None):
        lcd     =   lcd or self.__default_lcd

        lcd.clear()

        self._draw_title(self.title.text)

        for i in range(self.__window_position, len(self.__items)):
            font_height     =   self.__font['HEIGHT']
            title_height    =   self.title._height
            queue_number_in_window = i - self.__window_position

            y               =   queue_number_in_window * font_height + title_height + 2 + self.__padding * i

            # special condition to make it look better when using vietnamese font
            if (self.__font == font_vi) and (queue_number_in_window > 0):
                y += 3

            if (y + font_height >= LCD_HEIGHT):
                break
            try:
                lcd.put_string(str(self.__items[i]), x = 2, y = y)
            except TypeError:
                print(len(self.__items))
                print(type(self.__items[-1].title.text))
                lcd.put_string(self.title.text, x = 2, y = y)

        self.__draw_arrow_menu()
        self.__invert_current_item()

        lcd.refresh()

    def start(self, lcd = None, buttons = None):
        lcd     =   lcd     or self.__default_lcd
        buttons =   buttons or self.__default_buttons

        self.show()

        while(1):
            if (buttons[0].is_pressed):
                wait_button(buttons[0])
                pass

            if (buttons[1].is_pressed):
                self._arrow_up.invert()
                wait_button(buttons[1])

                self.__up()

            if (buttons[2].is_pressed):
                self._arrow_down.invert()
                wait_button(buttons[2])
                
                self.__down()

            if (buttons[3].is_pressed):
                wait_button(buttons[3])
                if (self.__current_item == 0):
                    # if has new update, load new update before opening menu list
                    if (self.has_new_menu):
                        self.update_menu()

                    self.__items[0].show()

                    self.show()

                if (self.__current_item == 1):
                    self.__show_order_list()

                    self.show()        

                if (self.__current_item == 2):
                    self.__show_receipt()

                    self.show()

                if (self.__current_item == 3):
                    self.__show_qr()

                    self.show()

                # quit
                if (self.__current_item == 4):
                    lcd.clear()
                    lcd.refresh()
                    break

    #####################################

    def get_item(self, index = 0, is_current = False, is_last = False):
        if (is_current):
            return self.__items[self.__current_item]
        elif (is_last):
            return self.__items[-1]
        else:
            return self.__items[index]

    def add_item(self, item_name):
        from MenuItem import MenuItem

        self.__items.append(MenuItem(  item_name        =   item_name,
                                       default_lcd      =   self.__default_lcd,
                                       default_buttons  =   self.__default_buttons,
                                       font             =   self.__font,
                                       padding          =   self.__padding))

    def set_lcd(self, lcd):
        self.__default_lcd  =   lcd

    def set_table_id(self, table_id):
        self.__table_id =   table_id

    def get_table_id(self):
        return self.__table_id

    def update_menu(self):
        # drop old items
        self.__items[0]._drop_table()

        try:
            # load new, updated items into menu list
            with open('./data/menu.json') as f:
                menu_data   =   json.load(f)
        except json.decoder.JSONDecodeError:
            menu_data = {}

        self.__load_menu(menu_data)

        self.has_new_menu = False

    #####################################

    def __up(self, lcd = None):
        lcd     =   lcd or self.__default_lcd

        if (self.__current_item > 0):

            self.__current_item -= 1

            # if current item is out of the view, move the window upwards
            self.__window_position = min(self.__window_position, self.__current_item)

            self.show()
        else:
            self.show()
            return

    def __down(self, lcd = None):
        lcd     =   lcd or self.__default_lcd

        if (self.__current_item < len(self.__items) - 1):

            if (self.__is_out_of_screen(self.__current_item + 1)):
                # if current item is out of the view, move the window downwards
                self.__window_position += 1

            self.__current_item += 1

            self.show()
        else:
            self.show()
            return

    def __show_receipt(self, lcd = None, buttons = None, index = 0):
        # receipt log only contains item ids and amount received, so we must look their names and price up

        lcd             =   lcd     or self.__default_lcd
        buttons         =   buttons or self.__default_buttons

        is_last_page    =   False

        index           =   max(0, index)

        lcd.clear()

        self._draw_title('Hoá Đơn')

        receipt         =   self.model.read_receipt()
        has_items       =   len(receipt) > 0

        if (index >= len(receipt)):
            index           =   len(receipt)
            is_last_page    =   True

        if (has_items) and (not is_last_page):
            try:
                menu            =   self.model.read_menu()

                this_order      =   receipt[index]

                item_id         =   this_order['item_id']
                this_item       =   menu[item_id]

                item_name       =   this_item['name']
                price           =   int(this_item['price'])
                amount_received =   int(this_order['amount'])
            except IndexError:
                print('index error at show receipt')

        self.__draw_arrow_dialog(receipt, index)

        if (not has_items):
            lcd.put_string('Chưa có gì :(', is_center = True, y = 15, font = font_vi)
        elif (not is_last_page):
            self.__draw_receipt_page(item_name, price, amount_received)
            pass
        else:
            # else it must be the last page
            self.__draw_receipt_total(receipt)

        lcd.refresh()

        while(1):
            if (buttons[0].is_pressed):
                wait_button(buttons[0])

                break

            if (buttons[1].is_pressed):
                self._arrow_up.invert()
                wait_button(buttons[1])

                self.__show_receipt(index = index - 1)
                break

            if (buttons[2].is_pressed):
                self._arrow_down.invert()
                wait_button(buttons[2])

                self.__show_receipt(index = index + 1)
                break

    def __show_order_list(self, lcd = None, buttons = None, index = 0):
        # order list only contains item ids and amount waiting, so we must look their names and price up

        lcd             =   lcd     or self.__default_lcd
        buttons         =   buttons or self.__default_buttons

        is_last_page    =   False

        index           =   max(0, index)

        lcd.clear()

        self._draw_title('Xác nhận order')

        order_list      =   self.model.read_order_list()
        has_items       =   len(order_list) > 0

        if (index >= len(order_list)):
            index           =   len(order_list)
            is_last_page    =   True

        # if the order list is not empty, read it
        if (has_items) and (not is_last_page):
            try:
                menu            =   self.model.read_menu()

                this_order      =   order_list[index]

                item_id         =   this_order['item_id']
                this_item       =   menu[item_id]

                item_name       =   this_item['name']
                amount_left     =   int(this_item['left'])
                amount_waiting  =   int(this_order['amount'])

            except IndexError:
                print('index error at show order list')

        self.__draw_arrow_dialog(order_list, index)

        if (not has_items):
            lcd.put_string('Chưa có gì :(', is_center = True, y = 15, font = font_vi)
        elif (not is_last_page):
            self.__draw_order_page(item_name, amount_waiting)
        else:
            # else it must be the last page
            self.__draw_confirm_dialog()

        lcd.refresh()

        while(1):
            if (buttons[0].is_pressed):
                wait_button(buttons[0])
                break

            if (buttons[1].is_pressed):
                self._arrow_up.invert()
                wait_button(buttons[1])

                self.__show_order_list(index = index - 1)
                break

            if (buttons[2].is_pressed):
                self._arrow_down.invert()
                wait_button(buttons[2])

                self.__show_order_list(index = index + 1)
                break

            if (buttons[3].is_pressed):
                wait_button(buttons[3])

                if (has_items and not is_last_page):
                    new_amount = self.__change_order(waiting = amount_waiting, left = amount_left)

                    this_order['amount'] = new_amount

                    # write back updated order data
                    self.model.write_order_list(order_list)

                    self.__show_order_list(index = index)

                    break

                if (has_items and is_last_page):
                    self.__interact_confirm_dialog()
                    break
                    
    def __change_order(self, waiting, left, lcd = None, buttons = None):
        from fonts.font10x10    import font10x10
        from pcd8544.lcd        import LCD_WIDTH, WHITE

        lcd             =   lcd     or self.__default_lcd
        buttons         =   buttons or self.__default_buttons

        CHAR_WIDTH      =   10
        CHAR_HEIGHT     =   10
        total_width     =   (len('-{}+'.format(waiting))) * (CHAR_WIDTH + 1) # 1 = space between each char

        font_height     =   self.__font['HEIGHT']
        title_height    =   self.title._height
        y               =   title_height + 2 + self.__padding + font_height + 2

        # calculate where the '-' and '+' char are, in order to invert it
        x_minus         =   (LCD_WIDTH - total_width) // 2 
        x_plus          =   (LCD_WIDTH + total_width) // 2 

        # only allow to increase the value
        original_value  =   waiting


        lcd.put_string('-{}+'.format(waiting), y = y, is_center = True, font = font10x10)
        lcd.refresh()

        while (1):
            if (buttons[0].is_pressed):
                wait_button(buttons[0])

                return original_value

            if (buttons[1].is_pressed):
                # invert '-' char
                lcd.invert_rect(x1 = x_minus, y1 = y, x2 = x_minus + CHAR_WIDTH - 1, y2 = y + CHAR_HEIGHT - 1)
                lcd.refresh()

                wait_button(buttons[1])

                if (waiting > 0):
                    waiting -= 1

                lcd.fill_rect(x1 = 1, y1 = y, x2 = 82, y2 = y + CHAR_HEIGHT - 1, color = WHITE)
                lcd.put_string('-{}+'.format(waiting), y = y, is_center = True, font = font10x10)
                lcd.refresh()

            if (buttons[2].is_pressed):
                # invert '+' char
                lcd.invert_rect(x1 = x_plus - 2, y1 = y, x2 = x_plus - CHAR_WIDTH - 1, y2 = y + CHAR_HEIGHT - 1)
                lcd.refresh()

                wait_button(buttons[2])

                if (waiting < left):
                    waiting += 1

                lcd.fill_rect(x1 = 1, y1 = y, x2 = 82, y2 = y + CHAR_HEIGHT - 1, color = WHITE)
                lcd.put_string('-{}+'.format(waiting), y = y, is_center = True, font = font10x10)
                lcd.refresh()

            if (buttons[3].is_pressed):
                wait_button(buttons[0])

                return waiting

    def __show_qr(self, lcd = None, buttons = None):
        import pyqrcode
        from pcd8544.lcd    import BLACK, WHITE, LCD_HEIGHT, LCD_WIDTH

        lcd         =   lcd     or self.__default_lcd
        buttons     =   buttons or self.__default_buttons

        clear_text  =   self.__get_order_data()
        qr_string   =   pyqrcode.create(clear_text, error = 'H').text()
        edge_length =   0

        for char in qr_string:
            if (char == '\n'):
                break
            else:
                edge_length += 1

        lcd.clear()

        x0 = (LCD_WIDTH - edge_length) // 2
        y0 = (LCD_HEIGHT - edge_length) // 2

        x = x0
        y = y0

        for char in qr_string:
            if (char == '\n'):
                x = x0
                y = y + 1
                continue
            else:
                color = BLACK if (char == '1') else WHITE
                lcd.set_pixel(x = x, y = y, color = color)
                x += 1

        lcd.draw_rect(0, 0, 83, 47)
        lcd.refresh()

        while (1):
            if (buttons[0].is_pressed):
                wait_button(buttons[0])

                return

    #####################################

    def __is_out_of_screen(self, current_item):
        font_height     =   self.__font['HEIGHT']
        title_height    =   self.title._height
        current_item_position   =   current_item - self.__window_position

        y1              =   current_item_position * font_height + title_height + 2
        y2              =   y1 + font_height

        if (y1 > LCD_HEIGHT) or (y2 > LCD_HEIGHT):
            return True

        return False

    def __draw_order_page(self, name, amount_waiting, lcd = None):
        from fonts.font10x10 import font10x10

        lcd             =   lcd or self.__default_lcd

        font_height     =   self.__font['HEIGHT']
        title_height    =   self.title._height
        y               =   title_height + 2 + self.__padding

        lcd.put_string('+ {}:'.format(name), x = 2, y = y, font = self.__font)
        lcd.put_string(' {} '.format(amount_waiting), y = y + font_height + 2, is_center = True, font = font10x10)

    def __draw_receipt_page(self, name, amount_received, price, lcd = None):
        lcd             =   lcd or self.__default_lcd

        font_height     =   self.__font['HEIGHT']
        title_height    =   self.title._height
        y               =   title_height + 2 + self.__padding

        cost            =   price * amount_received

        # show order infos
        lcd.put_string('{} x {}'.format(price, amount_received), x = 10, y = y + font_height - 2, font = font_vi)
        lcd.put_string('{}'.format(cost), x = 10, y = y + font_height + 9, font = font_vi)

        lcd.draw_horizontal_line(x1 = 6, x2 = 60, y = y + font_height + 10)

        lcd.put_string('+ {}:'.format(name), x = 2, y = y, font = font_vi)

    def __draw_receipt_total(self, receipt, lcd = None):
        lcd         =   lcd or self.__default_lcd

        total_cost  =   0

        receipt     =   self.model.read_receipt()
        menu        =   self.model.read_menu()

        for order in receipt:
            item_id =   order['item_id']
            amount  =   order['amount']
            price   =   menu[item_id]['price']

            total_cost  += int(amount) * int(price)

        lcd.put_string('Tổng thiệt hại:', x = 3, y = 13, font = font_vi)
        lcd.put_string('{}'.format(total_cost), x = 3, y = 25, font = font_vi)

    # Draw the static GUI in confirm dialog
    def __draw_confirm_dialog(self, lcd = None, buttons = None):
        lcd             =   lcd     or self.__default_lcd
        buttons         =   buttons or self.__default_buttons

        button_accept   =   [0xFE, 0x01, 0x01, 0x01, 0x01, 0x11, 0x31, 0x61, 0x39, 0x0D,
                             0x01, 0x01, 0x01, 0x01, 0xFE, 0x00, 0x01, 0x01, 0x01, 0x01,
                             0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00]

        button_decline  =   [0xFE, 0x01, 0x01, 0x01, 0x45, 0x6D, 0x39, 0x11, 0x39, 0x6D,
                             0x45, 0x01, 0x01, 0x01, 0xFE, 0x00, 0x01, 0x01, 0x01, 0x01,
                             0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00]

        BUTTON_HEIGHT   =   9
        BUTTON_WIDTH    =   15

        x_accept        =   53
        x_decline       =   15

        y_button        =   30

        lcd.put_string('  Đặt món:', y = 13, is_center = True, font = font_vi)

        lcd.draw_image(button_accept, x = x_accept, y = y_button, width = BUTTON_WIDTH, height = BUTTON_HEIGHT)
        lcd.draw_image(button_decline, x = x_decline, y = y_button, width = BUTTON_WIDTH, height = BUTTON_HEIGHT)

    # Control what the program should do with button clicks in confirm dialog
    def __interact_confirm_dialog(self, lcd = None, buttons = None):
        lcd             =   lcd     or self.__default_lcd
        buttons         =   buttons or self.__default_buttons

        BUTTON_HEIGHT   =   9
        BUTTON_WIDTH    =   15

        x_accept        =   53
        x_decline       =   15

        y_button        =   30

        # decline by default
        lcd.invert_rect(x1 = x_decline + 1, y1 = y_button + 1, width = BUTTON_WIDTH - 3, height = BUTTON_HEIGHT - 3)
        is_accept       =   False

        lcd.refresh()

        while (1):
            if (buttons[0].is_pressed):
                wait_button(buttons[0])

                break

            if (buttons[1].is_pressed):
                wait_button(buttons[1])

                is_accept = not is_accept
                lcd.invert_rect(x1 = x_decline + 1, y1 = y_button + 1, width = BUTTON_WIDTH - 3, height = BUTTON_HEIGHT - 3)
                lcd.invert_rect(x1 = x_accept + 1, y1 = y_button + 1, width = BUTTON_WIDTH - 3, height = BUTTON_HEIGHT - 3)

                lcd.refresh()

            if (buttons[2].is_pressed):
                wait_button(buttons[2])

                is_accept = not is_accept
                lcd.invert_rect(x1 = x_decline + 1, y1 = y_button + 1, width = BUTTON_WIDTH - 3, height = BUTTON_HEIGHT - 3)
                lcd.invert_rect(x1 = x_accept + 1, y1 = y_button + 1, width = BUTTON_WIDTH - 3, height = BUTTON_HEIGHT - 3)

                lcd.refresh()

            if (buttons[3].is_pressed):

                # invert whatever button is chosen
                if (is_accept):
                    lcd.invert_rect(x1 = x_accept + 1, y1 = y_button + 1, width = BUTTON_WIDTH - 3, height = BUTTON_HEIGHT - 3)
                else:
                    lcd.invert_rect(x1 = x_decline + 1, y1 = y_button + 1, width = BUTTON_WIDTH - 3, height = BUTTON_HEIGHT - 3)
                    print('declined')

                wait_button(buttons[3])

                if (is_accept):
                    import threading
                    payload     =   self.__get_order_data()

                    mqtt_thread =   threading.Thread(target = self.__send_mqtt, args = (payload,))

                    mqtt_thread.start()

                    # move all items from 'order list' to 'waiting list'
                    order_list      =   self.model.read_order_list()
                    waiting_list    =   self.model.read_waiting_list()

                    waiting_list    +=  order_list
                    self.model.write_waiting_list(waiting_list)
                    self.model.write_order_list([])

                break

    #####################################

    def _draw_title(self, title, lcd = None, font = None):
        font    =   font    or self.__font
        lcd     =   lcd     or self.__default_lcd

        lcd.draw_rect(0, 0, 83, 47)
        lcd.put_string(title, y = self.__padding, is_center = True, font = self.__font)
        lcd.invert_rect(1, 1, 82, self.title._height)

    def __draw_arrow_menu(self, lcd = None):
        lcd             =   lcd or self.__default_lcd

        is_last_item    =   self.__current_item == (len(self.__items) - 1)
        is_first_item   =   self.__current_item == 0

        if (is_last_item) and (is_first_item):
            # only one item, show nothing
            pass
        elif (is_last_item and not is_first_item):
            # have more than one item and at last item, show only arrow up
            self._arrow_up.draw()
        elif (is_first_item and not is_last_item):
            # have more than one item and at first item, show only arrow down
            self._arrow_down.draw()
        else:
            # else show both
            self._arrow_down.draw()
            self._arrow_up.draw()

    def __draw_arrow_dialog(self, item_list, index, lcd = None):
        lcd             =   lcd or self.__default_lcd

        has_items       =   len(item_list) > 0
        is_last_page    =   index >= len(item_list)
        is_first_page   =   index == 0

        # draw the arrows
        if (not has_items):
            # no items in receipt, show nothing
            pass
        elif (is_last_page and is_first_page):
            # if have only one page, show nothing
            pass
        elif (is_last_page and not is_first_page):
            # if have more than one page and at last page, show only arrow up
            self._arrow_up.draw()
        elif (is_first_page and not is_last_page):
            # if have more than one page and at first page, show only arrow down
            self._arrow_down.draw()
        else:
            # else show both
            self._arrow_down.draw()
            self._arrow_up.draw()

    def __invert_current_item(self, lcd = None):
        lcd                     =   lcd or self.__default_lcd

        font_height             =   self.__font['HEIGHT']
        title_height            =   self.title._height
        current_item_position   =   self.__current_item - self.__window_position

        y1                      =   current_item_position * font_height + title_height + 2
        y2                      =   y1 + font_height

        # special condition to make it look better when using vietamese font
        if (self.__font == font_vi) and (current_item_position > 0):
            y1 += 3
            y2 += 3

        if ((y1 > LCD_HEIGHT) or (y2 > LCD_HEIGHT)):
            return

        lcd.invert_rect(x1 = 2, y1 = y1, x2 = 77, y2 = y2)

    #####################################

    def __get_order_data(self):
        result      =   'T{}'.format(self.__table_id)
        order_list  =   self.model.read_order_list()

        for order in order_list:
            result += '#{}x{}'.format(order['item_id'], order['amount'])

        return result

    def __send_mqtt(self, payload):
        import paho.mqtt.publish    as  publish
        from global_variables  import  TOPIC_PUSH_ORDER, SERVER_ADDRESS

        try:
            publish.single(topic = TOPIC_PUSH_ORDER, payload = payload, hostname = SERVER_ADDRESS)
        except ConnectionRefusedError:
            print('Failed to send data. Have you started the mqtt server?')

    def __load_menu(self, menu_data):
        menu    =   self.__items[0]
        for item_id, item in menu_data.items():
            item_name   =   item['name']
            parent_id   =   item['parent']
            is_leaf     =   item['is_leaf']

            # top level item
            parent      =   menu if (parent_id is None) else menu.find_item(item_id = parent_id)

            if (is_leaf):
                item_price  =   item['price']
                item_left   =   item['left']

                parent.add_item(item_name = item_name, price = item_price, left = item_left, id = item_id)
            else:
                # else it must be a branch
                parent.add_item(item_name = item_name, id = item_id, is_leaf = False)

