import codecs
import json

def wait_button(button):
    while(button.is_pressed):
        pass

class Model:

    def __init__(self):
        pass

    @staticmethod
    def write_order_list(order_list):
        with codecs.open('logs/order_list.json', 'w', encoding = 'utf-8') as order_log:
            order_log.write(json.dumps(order_list, ensure_ascii = False, indent = 2))

    @staticmethod
    def read_order_list():
        try:
            # read log file as json data
            with codecs.open('logs/order_list.json', encoding = 'utf-8') as order_log:
                return json.load(order_log)
        except json.decoder.JSONDecodeError:
            # if log file empty, return empty list
            return []

    @staticmethod
    def write_waiting_list(waiting_list):
        with codecs.open('logs/waiting_list.json', 'w', encoding = 'utf-8') as order_log:
            order_log.write(json.dumps(waiting_list, ensure_ascii = False, indent = 2))    

    @staticmethod
    def read_waiting_list():
        try:
            # read log file as json data
            with codecs.open('logs/waiting_list.json', encoding = 'utf-8') as order_log:
                return json.load(order_log)
        except json.decoder.JSONDecodeError:
            # if log file empty, return empty list
            return []

    @staticmethod
    def read_menu():
        try:
            # read menu as json data
            with codecs.open('data/menu.json', encoding = 'utf-8') as menu:
                return json.load(menu)
        except json.decoder.JSONDecodeError:
            # if log file empty, return empty dict
            return {}

    @staticmethod
    def read_receipt():
        try:
            # read log file as json data
            with codecs.open('logs/receipt.json', encoding = 'utf-8') as receipt:
                return json.load(receipt)
        except json.decoder.JSONDecodeError:
            # if log file empty, return empty list
            return []

