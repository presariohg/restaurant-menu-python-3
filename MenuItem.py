from MenuBar        import  MenuBar
from MenuBarModel   import  wait_button

from pcd8544.fonts.font_vi  import font_vi

class MenuItem(MenuBar):
    def __init__(self, item_name, default_lcd, default_buttons, font = font_vi, padding = 0, price = 0, left = 0, id = 0, is_leaf = False):
        super(MenuItem, self).__init__( item_name          =   item_name, 
                                        default_lcd         =   default_lcd,
                                        default_buttons     =   default_buttons,
                                        font                =   font,
                                        padding             =   padding)

        self.__is_leaf          =   is_leaf

        self.__price            =   price
        self.__amount_left      =   left
        self.__id               =   id
        self.__amount_waiting   =   0
        self.__amount_delivered =   0

    def __str__(self):
        return self.title.text

    def add_item(self, item_name, price = 0, left = 0, id = 0, is_leaf = True):
        if (type(item_name) is MenuItem):
            # support adding a MenuItem directly here, but due to time shortage
            # I must use the same variable name 'item_name'
            self._MenuBar__items.append(item_name)
        else:
            self._MenuBar__items.append(MenuItem( item_name         =   item_name, 
                                                  default_lcd       =   self._MenuBar__default_lcd,
                                                  default_buttons   =   self._MenuBar__default_buttons,
                                                  price             =   price,
                                                  left              =   left,
                                                  id                =   id,
                                                  is_leaf           =   is_leaf))
        
        # this item has a child, so it must be a brach
        self.__is_leaf  =   False


    def show(self, lcd = None, buttons = None):
        lcd     =   lcd     or self._MenuBar__default_lcd
        buttons =   buttons or self._MenuBar__default_buttons

        super(MenuItem, self).show()
        if (self.__is_leaf):

            # read this session's data from file
            order_list  =   self.model.read_order_list()

            # search this item in order list
            for order in order_list:
                if (order['item_id'] == self.__id):
                    # if found, load its waiting amount, else the waiting amount should be 0 by default
                    self.__amount_waiting   =   order['amount']
                    break

            self.__draw_leaf()

            while (1):
                if (buttons[0].is_pressed):
                    wait_button(buttons[0])

                    self.__leaf_log_write()

                    break

                if (buttons[1].is_pressed):
                    # invert the '-' char as the user hold the button
                    self.__invert_minus_char(self.__amount_waiting)

                    wait_button(buttons[1])

                    if (self.__amount_waiting > 0):
                        self.__amount_waiting -= 1

                    self.__draw_leaf()

                if (buttons[2].is_pressed):
                    # invert the '+' char as the user hold the button
                    self.__invert_plus_char(self.__amount_waiting)

                    wait_button(buttons[2])

                    if (self.__amount_waiting < self.__amount_left):
                        self.__amount_waiting += 1

                    self.__draw_leaf()

        # else the menu item must be a branch
        else:
            self.__show_branch()

    def _drop_table(self):
        self._MenuBar__items.clear()

    def __show_branch(self, lcd = None, buttons = None):
        lcd     =   lcd     or self._MenuBar__default_lcd
        buttons =   buttons or self._MenuBar__default_buttons

        while (1):
            if (buttons[0].is_pressed):
                wait_button(buttons[0])

                break

            if (buttons[1].is_pressed):
                self._arrow_up.invert()
                wait_button(buttons[1])

                self.__up()
                break

            if (buttons[2].is_pressed):
                self._arrow_down.invert()
                wait_button(buttons[2])

                self.__down()
                break

            if (buttons[3].is_pressed):
                wait_button(buttons[3])

                if (not self.__is_leaf):
                    self.get_item(is_current = True).show()
                    # after exit sub menu, show current menu
                    self.show()
                    break

    def __draw_leaf(self, lcd = None):
        from fonts.font10x10 import font10x10

        lcd     =   lcd     or self._MenuBar__default_lcd

        lcd.clear()

        self._draw_title(self.title.text)
        
        # print current selected amount
        if (self.__amount_waiting == 0):
            lcd.put_string(' {}+'.format(self.__amount_waiting), y = 15, is_center = True, font = font10x10)
        elif (self.__amount_waiting == self.__amount_left):
            lcd.put_string('-{} '.format(self.__amount_waiting), y = 15, is_center = True, font = font10x10)
        else:
            lcd.put_string('-{}+'.format(self.__amount_waiting), y = 15, is_center = True, font = font10x10)


        lcd.put_string('Giá: {}'.format(self.__price), y = 26, is_center = True, font = self._MenuBar__font)
        lcd.put_string('Còn lại: {}'.format(self.__amount_left), y = 35, is_center = True, font = self._MenuBar__font)

        lcd.refresh()

    def __up(self, lcd = None, buttons = None):
        lcd     =   lcd     or self._MenuBar__default_lcd
        buttons =   buttons or self._MenuBar__default_buttons

        if (self._MenuBar__current_item > 0):

            self._MenuBar__current_item -= 1

            # if current item is out of the view, move the window upwards
            self._MenuBar__window_position = min(self._MenuBar__window_position, self._MenuBar__current_item)

            self.show()
        else:
            self.show()
            return

    def __down(self, lcd = None, buttons = None):
        if (self._MenuBar__current_item < len(self._MenuBar__items) - 1):

            if (self._MenuBar__is_out_of_screen(self._MenuBar__current_item + 1)):
                # if current item is out of the view, move the window downwards
                self._MenuBar__window_position += 1

            self._MenuBar__current_item += 1

            self.show()
        else:
            self.show()
            return

    #####################################

    def get_id(self):
        return self.__id

    def is_leaf(self):
        return self.__is_leaf

    def find_item(self, item_id):
        if (not self.__is_leaf):
            # if this item is a branch
            for item in self._MenuBar__items:
                # if this item is what we're looking for, return it
                if (item.get_id() == item_id):
                    return item

                # else if this item is not what we're looking for,
                # but it is a branch, recursive search
                elif (not item.is_leaf()):
                    result  =   item.find_item(item_id)
                    if (type(result) is MenuItem):
                        # if had found item, return it
                        return result
                    else:
                        # or else continue to the next item
                        continue

        else:
            # print(self.__id)
            if (self.__id == item_id):
                return self

    def __invert_minus_char(self, amount_waiting, lcd = None):
        # calculate where the '-' char is, in order to invert it
        from pcd8544.lcd    import LCD_WIDTH

        lcd          =  lcd or self._MenuBar__default_lcd

        CHAR_WIDTH   =  10
        CHAR_HEIGHT  =  10

        total_width  =  (len('-{}+'.format(amount_waiting))) * (CHAR_WIDTH + 1) # 1 = space between each char
        # x = (LCD_WIDTH - total_width) // 2 if (total_width < LCD_WIDTH) else 0
        x = (LCD_WIDTH - total_width) // 2
        lcd.invert_rect(x1 = x, y1 = 15, x2 = x + CHAR_WIDTH - 1, y2 = 15 + CHAR_HEIGHT - 1)
        lcd.refresh()

    def __invert_plus_char(self, amount_waiting, lcd = None):
        # calculate where the '+' char is, in order to invert it
        from pcd8544.lcd    import LCD_WIDTH

        lcd             =   lcd or self._MenuBar__default_lcd

        CHAR_WIDTH      =  10
        CHAR_HEIGHT     =  10

        total_width  =  (len('-{}+'.format(amount_waiting))) * (CHAR_WIDTH + 1) # 1 = space between each char
        # x = (LCD_WIDTH + total_width) // 2 if (total_width < LCD_WIDTH) else 83
        x = (LCD_WIDTH + total_width) // 2
        lcd.invert_rect(x1 = x - CHAR_WIDTH - 1, y1 = 15, x2 = x - 2, y2 = 15 + CHAR_HEIGHT - 1)
        lcd.refresh()

    def __leaf_log_write(self):
        # read old order list
        order_list      =   self.model.read_order_list()

        already_queuing =   False
        index_to_pop    =   None

        # write new / change this item's order data in order list
        for index, order in enumerate(order_list):
            # if this item is already in queue, change its order amount
            if (order['item_id'] == self.__id):
                order['amount']     =   self.__amount_waiting
                already_queuing     =   True

                # if this item already in queue, but now reduced to 0
                if (self.__amount_waiting == 0):
                    index_to_pop    =   index
                break

        if (not already_queuing) and (self.__amount_waiting > 0):
            order_data  =   {"item_id"  :   self.__id,
                             "amount"   :   self.__amount_waiting}

            order_list.append(order_data)

        # if this amount == 0, remove this item from order list
        if (index_to_pop != None):
            order_list.pop(index_to_pop)

        # write back updated order data
        self.model.write_order_list(order_list)
